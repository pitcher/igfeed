<?php
/**
 * Created by PhpStorm.
 * User: Pitcher - HP
 * Date: 19/08/2020
 * Time: 10:19
 */

namespace Pitcher\Instagram;


class Cronjob
{

    private $cronperiod = "daily";

    public function __construct()
    {
        $this->setupCronJobs();

        add_action('instagramCronJob', array($this, 'Dailycronjob'));
    }

    public function setupCronJobs()
    {
        wp_schedule_event(strtotime('00:00'), $this->cronperiod,  'instagramCronJob');
    }

    public function Dailycronjob()
    {
        (new Client())->extendLongLivedAuthToken();
    }
}
