<?php
/**
 * Created by PhpStorm.
 * User: Pitcher - HP
 * Date: 18/08/2020
 * Time: 09:16
 */

namespace Pitcher\Instagram;

use \EspressoDev\InstagramBasicDisplay\InstagramBasicDisplay;


/**
 * Class Client
 * @package App\Controllers\Instagram
 * Wrapper Class that Converts .env variables into InstagramClass.
 */
class Client
{
    private $appId = "";
    private $appSecret = "";
    private $redirectUri = "";
    private $code = "";
    private $longLivedToken = false;
    private $longLivedTokenExtensions = "";

    private $cachePrefix = "instagramApi_";
    private $expiration = 1;

    public function __construct()
    {
        $this->appId = env('INSTAGRAM_APPID') ? env('INSTAGRAM_APPID') : false;
        $this->appSecret = env('INSTAGRAM_APPSECRET') ? env('INSTAGRAM_APPSECRET') : false;
        $this->redirectUri = home_url() . "/";
        $this->expiration = env('INSTAGRAM_EXPIRATION') ? env('INSTAGRAM_EXPIRATION') : 60;
        $this->longLivedToken = $this->getLongLivedAuthToken();
        $this->code = $this->getCodeFromCache();
        $this->longLivedTokenExtensions = $this->getLongLivedTokenExtentionsFromCache();
        // add Listener to site
        add_action('wp_loaded', array($this, 'listener'));
        add_action('wp_loaded', array($this, 'manualRenewTokenListener'));

        // If Auth token cache has expired, prompt user to re-login, but only when in admin of WP.
        if (!$this->longLivedToken) {
            add_action('admin_notices', array($this, 'adminNotice'));
        } else {
            $this->token = $this->getLongLivedAuthToken();
        }


        //setup cronjob
        new Cronjob();


        // Display debugger. Remove later
//        add_action('admin_notices', array($this, 'debugger'));
    }

    function InstagramBasicDisplay()
    {
        $instagram = new InstagramBasicDisplay([
            'appId' => $this->appId,
            'appSecret' => $this->appSecret,
            'redirectUri' => $this->redirectUri
        ]);

        return $instagram;
    }

    function issetLongLivedAuthToken()
    {
        $transient_key = $this->cachePrefix . "_longLivedAuthToken";
        if (false === ($data = get_transient($transient_key))) {
            return false;
        }
        return true;
    }

    function getLongLivedAuthToken()
    {
        $transient_key = $this->cachePrefix . "_longLivedToken";
        if (false === ($data = get_transient($transient_key))) {
            return false;
        }
        return $data;
    }

    function extendLongLivedAuthToken()
    {
        if (!$this->getLongLivedAuthToken()) {
            return false;
        }

        $instagram = $this->InstagramBasicDisplay();

        $refreshedtoken = $instagram->refreshToken($this->getLongLivedAuthToken(), true);

        if ($refreshedtoken) {
            $this->setLongLivedToken($refreshedtoken);
            $this->increaseLongLivedTokenExtentionToCache();
        } else {
            $this->setLongLivedToken(false);
        }
        return true;
    }


    function getCodeFromCache()
    {
        $transient_key = $this->cachePrefix . "_code";
        if (false === ($data = get_transient($transient_key))) {
            return false;
        }
        return $data;
    }

    function getLongLivedTokenExtentionsFromCache()
    {
        $transient_key = $this->cachePrefix . "_extendedcount";
        if (false === ($data = get_transient($transient_key))) {
            return 0;
        }
        return intval($data);
    }

    function increaseLongLivedTokenExtentionToCache()
    {
        $count = intval($this->getLongLivedTokenExtentionsFromCache());
        $count = $count + 1;
        $transient_key = $this->cachePrefix . "_extendedcount";
        set_transient($transient_key, $count, 0);
    }


    function setCodeInCache($code)
    {
        $expiration = 60; // 1 minute;
        $transient_key = $this->cachePrefix . "_code";
        set_transient($transient_key, $code, $expiration);
    }

    function setLongLivedToken($token)
    {
        $expiration = 60 * 60 * 24 * 60;
        $transient_key = $this->cachePrefix . "_longLivedToken";
        set_transient($transient_key, $token, $expiration);
    }

    function getLoginUrl()
    {
        $instagram = $this->InstagramBasicDisplay();
        return $instagram->getLoginUrl();
    }

    function auth()
    {
        // Get instagramBasicDisplay Instance
        $instagram = $this->InstagramBasicDisplay();

        // Get the short lived access token (valid for 1 hour)
        $oAuthToken = $instagram->getOAuthToken($this->getCodeFromCache(), true);

        // Exchange this token for a long lived token (valid for 60 days)
        $longLivedToken = $instagram->getLongLivedToken($oAuthToken, true);

        // Set LongLivedToken
        $this->setLongLivedToken($longLivedToken);
    }

    function adminNotice()
    {
        ?>
        <div class="notice notice-alert">
            <p>Zo te zien is de koppeling met Instagram verlopen, log hier opnieuw in om de laatste Instagram Posts weer
                te tonen op je website.</p>
            <p>Log in met het Instagram account waarvan je de posts op de site wil tonen.</p>
            <a href="<?php echo $this->getLoginUrl() ?>">Klik hier om je Instagram account te koppelen</a>
        </div>
        <?php
    }

    function debugger()
    {
        ?>
        <div class="notice notice-alert">
           <pre>
               <?php var_dump($this); ?>
           </pre>
        </div>
        <?php
    }

    function debug()
    {
        echo ' 
        <div class="notice notice-alert">
           <pre>
               ' . var_dump($this) . '
           </pre>
        </div>';
    }

    /*
     * This function listens to the home page, and only if a combination of auth tokens is set we activate the listener's internal function
     */
    function listener()
    {
        // only open listener when the longlivedcode is not set
        if (!$this->issetLongLivedAuthToken()) {
            // required field
            // code
            if (isset($_GET['code'])) {
                $this->code = sanitize_text_field($_GET['code']);
                $this->setCodeInCache($this->code);

                echo $this->debug();
                // start auth process
                $this->auth();

            }
        }
    }

    function manualRenewTokenListener()
    {

            if (isset($_GET['renew'])) {

                var_dump($this->getLongLivedAuthToken());

               $renewed = $this->extendLongLivedAuthToken();

               var_dump("renewed:");
               var_dump($renewed);
               if(!$renewed){
                   $this->setLongLivedToken(false);
               }

                var_dump($this->getLongLivedAuthToken());

                var_dump($this->getLongLivedTokenExtentionsFromCache());
            }



    }

    function getLatestPosts()
    {
        if (!$this->longLivedToken) {
            return false;
        }

        $transient_key = $this->cachePrefix . "_getLatestPosts";
        if (false === ($items = get_transient($transient_key))) {
            $instagram = New InstagramBasicDisplay($this->longLivedToken);
            $items = $instagram->getUserMedia();
            set_transient($transient_key, $items, $this->expiration);
            if (isset($items->data) && count($items->data) > 0) {
                return $items->data;
            } else {
                return false;
            }
        }
        if (isset($items->data) && count($items->data) > 0) {
            return $items->data;
        } else {
            return false;
        }
    }
}
